class GeneratorHomePage < GeneratorBasePage
  path '/'

  validate :title, 'Croove - QA Engineer'
  validate :url, %r{\Ahttps?://[^/]+/\z}

  element :info_link, '.btn[href="/info"]'
  element :see_a_number_link, '.btn[href="/result"]'

  def click_info_link
    info_link_element.click
  end

  def click_see_a_number_link
    see_a_number_link_element.click
  end
end
