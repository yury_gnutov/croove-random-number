class GeneratorBasePage < Howitzer::Web::Page
  site Howitzer.base_url

  def wait_for_jquery
    retryable(timeout: 30) do
      raise unless jquery_active?
    end
  end

  private

  def jquery_active?
    evaluate_script('jQuery.active').zero?
  end
end
