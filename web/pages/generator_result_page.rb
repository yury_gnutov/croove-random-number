class GeneratorResultPage < GeneratorBasePage
  path '/result'

  validate :title, 'Welcome to Croove - Number'
  validate :url, %r{\Ahttps?://[^/]+/result\z}

  element :back_home_link, '.btn[href="/"]'
  element :another_number_link, '.btn[href="/result"]'
  element :generated_number_message, '.result'

  def click_another_number_link
    another_number_link_element.click
    wait_for_jquery
  end

  def click_back_home_link
    back_home_link_element.click
  end

  def today_number
    generated_number_message_element.text.gsub(/\D+/, '\1').to_i
  end
end
