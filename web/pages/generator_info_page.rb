class GeneratorInfoPage < GeneratorBasePage
  validate :url, %r{\Ahttps?://[^/]+/info\z}
end
