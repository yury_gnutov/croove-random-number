require 'spec_helper'

RSpec.feature 'The number generator' do
  scenario 'must have a home page, incl. Links to info and the number result page' do
    GeneratorHomePage.open
    expect(GeneratorHomePage.given.has_info_link_element?).to be_truthy
    expect(GeneratorHomePage.given.has_see_a_number_link_element?).to be_truthy
  end

  scenario 'home page link to info opens up info page' do
    GeneratorHomePage.open
    GeneratorHomePage.given.click_info_link
    GeneratorInfoPage.given
  end

  scenario 'home page link to result page opens up result page' do
    GeneratorHomePage.open
    GeneratorHomePage.given.click_see_a_number_link
    GeneratorResultPage.given
  end

  scenario 'result page shows a randomly generated number between (inclusive) 0 and 99' do
    GeneratorResultPage.open
    expect(0..99).to include(GeneratorResultPage.given.today_number)
  end

  scenario 'result page must link back to the home page' do
    GeneratorResultPage.open
    expect(GeneratorResultPage.given.has_back_home_link_element?).to be_truthy
  end

  scenario 'result page link to home page leads to home page' do
    GeneratorResultPage.open
    GeneratorResultPage.given.click_back_home_link
    GeneratorHomePage
  end

  scenario 'result page must show another number on request' do
    GeneratorResultPage.open
    expect { GeneratorResultPage.given.click_another_number_link }.to(change { GeneratorResultPage.given.today_number })
  end
end
