# README #

Croove random numbers page

## Requirements

+ *nix system
+ ruby v. =>2.3.*
+ chromedriver
+ `gem install bundler`
+ `bundle install`

## How to run

+ `rspec /spec`

## Contribution

+ check yourself with `rubocop`